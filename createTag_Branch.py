import requests 
import csv

tag = "v2.0.0"

with open('csfile.csv') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        
        # Creates Tag
        path_Tag = 'https://gitlab.com/api/v4/projects/{}/repository/tags'.format(row['ProjectID'])

        headers_Tag = {
        'PRIVATE-TOKEN': 'bq5UkF3jsqRmduzDAv1r',
        'username': 'hj'  
        }
        params_Tag = (
            ('tag_name', tag),
            ('ref', 'master'),
        )

        response_Tag = requests.request("POST", path_Tag, headers=headers_Tag, params=params_Tag)
        print(response_Tag.text.encode('utf8'))

        # Creates Branch
        path_Branch = 'https://gitlab.com/api/v4/projects/{}/repository/branches'.format(row['ProjectID'])
        
        headers_Branch = {
        'PRIVATE-TOKEN': 'bq5UkF3jsqRmduzDAv1r',
        'username': 'hj'  
        }
        params_Branch = (
            ('branch', tag),
            ('ref', tag),
        )
      
        response_Branch = requests.request("POST", path_Branch, headers=headers_Branch, params=params_Branch)
        print(response_Branch.text.encode('utf8'))
