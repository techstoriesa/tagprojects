import requests

getlookuptypes = "https://gitlab.com/api/v4/projects/19928614/repository/branches"
savelookuptypes = "https://gitlab.com/api/v4/projects/19928607/repository/branches"
updatesimstatus = "https://gitlab.com/api/v4/projects/20000387/repository/branches"
querySimUpload = "https://gitlab.com/api/v4/projects/19929070/repository/branches"
simupload = "https://gitlab.com/api/v4/projects/19862586/repository/branches"
adduserapi = "https://gitlab.com/api/v4/projects/20648420/repository/branches"
billingaccountlistener = "https://gitlab.com/api/v4/projects/20898539/repository/branches"
modifycustomerapi = "https://gitlab.com/api/v4/projects/19552501/repository/branches"
quoteorderscheduler = "https://gitlab.com/api/v4/projects/20383281/repository/branches"
saveorder = "https://gitlab.com/api/v4/projects/21272102/repository/branches"
queryorder = "https://gitlab.com/api/v4/projects/21272067/repository/branches"
orderlistener = "https://gitlab.com/api/v4/projects/20898532/repository/branches"
updateorderapi = "https://gitlab.com/api/v4/projects/20337112/repository/branches"
getactivelinesapi = "https://gitlab.com/api/v4/projects/20043003/repository/branches"
submitorder = "https://gitlab.com/api/v4/projects/19305900/repository/branches"
validate = "https://gitlab.com/api/v4/projects/19305889/repository/branches"
orderportalproxy = "https://gitlab.com/api/v4/projects/20916891/repository/branches"
getjasperaccounts = "https://gitlab.com/api/v4/projects/20125582/repository/branches"
iotcustomerorderportal = "https://gitlab.com/api/v4/projects/20361124/repository/branches"
iotorderportal = "https://gitlab.com/api/v4/projects/19037510/repository/branches"
getcommplans = "https://gitlab.com/api/v4/projects/18644648/repository/branches"
savecommplans = "https://gitlab.com/api/v4/projects/18644634/repository/branches"
getrateplansapi = "https://gitlab.com/api/v4/projects/18783763/repository/branches"
saverateplans = "https://gitlab.com/api/v4/projects/18644606/repository/branches"
getrateplans = "https://gitlab.com/api/v4/projects/18644592/repository/branches"
publishproductconfiguration = "https://gitlab.com/api/v4/projects/18644672/repository/branches"
getproductconfiguration = "https://gitlab.com/api/v4/projects/18644529/repository/branches"
saveproductconfiguration = "https://gitlab.com/api/v4/projects/18644510/repository/branches"
iotproductconfigurator = "https://gitlab.com/api/v4/projects/18720154/repository/branches"
getapitokens = "https://gitlab.com/api/v4/projects/19468592/repository/branches"

tag = "DemoTags"

headers = {
  'PRIVATE-TOKEN': '',
  'username': ''  
}
params = (
    ('branch', tag),
    ('ref', tag),
)

response = requests.request("POST", getlookuptypes, headers=headers, params=params)
response = requests.request("POST", savelookuptypes, headers=headers, params=params)
response = requests.request("POST", updatesimstatus, headers=headers, params=params)
response = requests.request("POST", querySimUpload, headers=headers, params=params)
response = requests.request("POST", simupload, headers=headers, params=params)
response = requests.request("POST", adduserapi, headers=headers, params=params)
response = requests.request("POST", billingaccountlistener, headers=headers, params=params)
response = requests.request("POST", modifycustomerapi, headers=headers, params=params)
response = requests.request("POST", quoteorderscheduler, headers=headers, params=params)
response = requests.request("POST", saveorder, headers=headers, params=params)
response = requests.request("POST", queryorder, headers=headers, params=params)
response = requests.request("POST", orderlistener, headers=headers, params=params)
response = requests.request("POST", updateorderapi, headers=headers, params=params)
response = requests.request("POST", getactivelinesapi, headers=headers, params=params)
response = requests.request("POST", submitorder, headers=headers, params=params)
response = requests.request("POST", validate, headers=headers, params=params)
response = requests.request("POST", orderportalproxy, headers=headers, params=params)
response = requests.request("POST", getjasperaccounts, headers=headers, params=params)
response = requests.request("POST", iotcustomerorderportal, headers=headers, params=params)
response = requests.request("POST", iotorderportal, headers=headers, params=params)
response = requests.request("POST", getcommplans, headers=headers, params=params)
response = requests.request("POST", savecommplans, headers=headers, params=params)
response = requests.request("POST", getrateplansapi, headers=headers, params=params)
response = requests.request("POST", saverateplans, headers=headers, params=params)
response = requests.request("POST", getrateplans, headers=headers, params=params)
response = requests.request("POST", publishproductconfiguration, headers=headers, params=params)
response = requests.request("POST", getproductconfiguration, headers=headers, params=params)
response = requests.request("POST", saveproductconfiguration, headers=headers, params=params)
response = requests.request("POST", iotproductconfigurator, headers=headers, params=params)
response = requests.request("POST", getapitokens, headers=headers, params=params)

print(response.text.encode('utf8'))