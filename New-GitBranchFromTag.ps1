function New-GitBranchFromTag() {

    param(
        [parameter(Mandatory = $true)]
        [String]$CsvPath,
        [parameter(Mandatory = $true)]
        [String]$PersonalAccessToken,
        [String]$TagsOverWrite
    )

    $csv = Import-Csv -Path $CsvPath

    $csv | ForEach-Object {

        if (!$TagsOverWrite) {
            $TagsOverWrite = $_.Tag
        }

        $branchApi = "https://gitlab.com/api/v4/projects/$($_.ProjectID)/repository/branches"

        $body = @{
            branch = $TagsOverWrite;
            ref = $TagsOverWrite
        }

        Invoke-RestMethod -Method Post -Headers @{"PRIVATE-TOKEN" = "$PersonalAccessToken" } -Uri $branchApi -Body $body
    }

}