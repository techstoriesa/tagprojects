function Set-ProjectTags() {

    param(
        [parameter(Mandatory = $true)]
        [String]$CsvPath,
        [parameter(Mandatory = $true)]
        [String]$PersonalAccessToken,
        [String]$TagsOverWrite,
        [String]$ReleaseOverWrite,
        [Switch]$Release = $false,
        [Switch]$CreateBranch = $false
    )

    $csv = Import-Csv -Path $CsvPath

    $csv | ForEach-Object {

        if (!$TagsOverWrite) {
            $TagsOverWrite = $_.Tag
        }

        if ($Release) {
            $releaseUrl = "https://gitlab.com/api/v4/projects/$($_.ProjectID)/releases"

            if ($_.description) {
                $description = $_.description
            }
            else {
                $description = "Release created by autoamtion script"
            }

            if ($ReleaseOverWrite) {
                $releaseName = $ReleaseOverWrite
            }
            else {
                if ($_.release) {
                    $releaseName = $_.release
                }
                else {
                    $releaseName = $TagsOverWrite
                }
            }

            $body = @{
                description = $description;
                tag_name    = $TagsOverWrite;
                ref         = "master";
                name        = $releaseName
            }

            Invoke-RestMethod -Method Post -Headers @{"PRIVATE-TOKEN" = "$PersonalAccessToken" } -Uri $releaseUrl -Body $body

        }
        elseif (!$Release) {

            $tagsUrl = "https://gitlab.com/api/v4/projects/$($_.ProjectID)/repository/tags"

            if (!$TagsOverWrite) {
                $TagsOverWrite = $_.Tag
            }

            $body = @{
                tag_name = "$TagsOverWrite";
                ref      = "master"
            }

            Invoke-RestMethod -Method Post -Headers @{ 'PRIVATE-TOKEN' = "$PersonalAccessToken" } -Uri $tagsUrl -Body $body
        }

        if($CreateBranch) {

            Write-host "Creating branch from the tag."
            . ./New-GitBranchFromTag.ps1
            New-GitBranchFromTag -CsvPath $CsvPath -PersonalAccessToken $PersonalAccessToken
        }
    }

}